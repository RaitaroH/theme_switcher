# theme switcher [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-285959.svg)](https://gitlab.com/vednoc/theme_switcher/raw/master/switcher.user.css)

A simple implementation of color-scheme switcher for my userstyles. Big thank you to [RaitaroH](https://github.com/RaitaroH) for the color-schemes and the idea of how to implement custom colors within `select` UserCSS setting. :heart:

To use it, simply click on "Install directly with Stylus" button then head over to my [userstyles](https://gitlab.com/vednoc/userstyles) project to install userstyles you'd like to use.
